# CS Trivia


**CS Trivia Night!** was an event hosted and created by the St. Edward's Computer Science Club (CSC) in Fall 2019. 

**Contributors/CSC Officers:**

- Simon (me)

- Josh 

- Evangaline

- Ruben


## Description
Who doesn't like trivia?! This trivia comes with a twist. It's all about computer science related topics. So expect there to be questions about *Python Concepts & Code, Computer Networking, Algorithms and Data Structures, Database Concepts, Computer Security, and some other general categories like movies/TV, and games.* This will challenge every aspect of your inner-geek!

## Purpose

The main purpose was to educate students while having fun at it.  

## Files

Included in this repo is a PowerPoint and OpenDocument documents. **Answers are included in the notes section.**

## Example Question

### Algorithms & Data Structures

*Starts at the tree root and explores all of the neighbor nodes at the present depth prior to moving on to the nodes at the next depth level*

**a. Depth first search**

**b. Breadth first search**

**c. Prior search**

**d. Binary search**